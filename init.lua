vim.loader.enable()

require('config.options')
require('config.autoload')
require('config.keymaps')
require('config.plugins')
require('config.plugconf')
