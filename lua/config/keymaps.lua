local opts = { noremap = true, silent = true }

--
-- remap space as leader key
--
-- setting mapleader to space makes things messy with alignmaps plugins
--
--vim.keymap.del({}, ' ', nil)
--vim.g.mapleader = ' '
--vim.g.maplocalleader = ' '


--
-- keymaps for navigation
--

-- window movements
vim.keymap.set({ 'n' }, '<C-z>', '<Plug>IMAP_JumpForward', opts) -- preserve <C-j>

vim.keymap.set({ 'n' }, '<C-h>', '<C-w>h', opts)
vim.keymap.set({ 'n' }, '<C-j>', '<C-w>j', opts)
vim.keymap.set({ 'n' }, '<C-k>', '<C-w>k', opts)
vim.keymap.set({ 'n' }, '<C-l>', '<C-w>l', opts)

-- window resizing
vim.keymap.set({ 'n' }, '<C-Up>', ':resize -2<CR>', opts)
vim.keymap.set({ 'n' }, '<C-Down>', ':resize +2<CR>', opts)
vim.keymap.set({ 'n' }, '<C-Right>', ':vertical resize +2<CR>', opts)
vim.keymap.set({ 'n' }, '<C-Left>', ':vertical resize -2<CR>', opts)

-- buffer movements
vim.keymap.set({ 'n' }, '<S-l>', ':bnext<CR>', opts)
vim.keymap.set({ 'n' }, '<S-h>', ':bprevious<CR>', opts)
vim.keymap.set({ 'n' }, '<S-B>', ':buffers<CR>', opts)

-- buffer operations
vim.keymap.set({ 'n' }, '<C-x>x', ':bd!<CR>', opts)

-- terminal operations

vim.keymap.set('n', '<C-x>l', function()
    if vim.bo.buftype ~= 'terminal' then
        return;
    end
    vim.opt_local.scrollback = 1
    vim.api.nvim_command("startinsert")
    vim.api.nvim_feedkeys("clear", 't', false)
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<cr>', true, false, true), 't', true)
    vim.opt_local.scrollback = 10000
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<C-\\><C-n>', true, false, true), 't', true)
    vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('gg', true, false, true), 't', true)
end, opts)



-- file explorer
--vim.keymap.set({ 'n' }, '<space>e', ':Lex<CR>', opts)

--
-- keymaps for editing
--

-- indentation
vim.keymap.set({ 'v' }, '<', '<gv', opts)
vim.keymap.set({ 'v' }, '>', '>gv', opts)

-- moving lines
vim.keymap.set({ 'v' }, '<A-j>', ":move '>+1<CR>gv-gv", opts)
vim.keymap.set({ 'v' }, '<A-k>', ":move '<-2<CR>gv-gv", opts)


--
-- keymaps for plugins
--

-- dap
local dap_ok, dap = pcall(require, 'dap')
local dapui_ok, dapui = pcall(require, 'dapui')

if dapui_ok then
    vim.keymap.set({ 'n' }, '<M-k>', function() dapui.eval() end, opts)
else
    vim.notify('DAP UI plugin not found, not assigning keymaps', vim.log.levels.ERROR, nil)
end

if dap_ok then
    vim.keymap.set({ 'n' }, '<F21>', function() dap.continue() end, opts) -- Shift+F9
    vim.keymap.set({ 'n' }, '<F9>', function() dap.continue() end, opts)
    vim.keymap.set({ 'n' }, '<F8>', function() dap.step_over() end, opts)
    vim.keymap.set({ 'n' }, '<F7>', function() dap.step_into() end, opts)
    vim.keymap.set({ 'n' }, '<F20>', function() dap.step_out() end, opts)          -- Shift+F8
    vim.keymap.set({ 'n' }, '<F32>', function() dap.toggle_breakpoint() end, opts) -- Ctrl+F8
    -- vim.keymap.set({ 'n' }, '<F33>', function() dap.run_to_cursor() end, opts)     -- Ctrl+F9
    vim.keymap.set({ 'n' }, '<F14>', function() dap.terminate() end, opts)         -- Shift+F2
else
    vim.notify('DAP plugin not found, not assigning keymaps', vim.log.levels.ERROR, nil)
end

-- telescope
local telescope_ok, builtin = pcall(require, 'telescope.builtin')
if telescope_ok then
    vim.keymap.set('n', '<space>ff', builtin.find_files, opts)
    vim.keymap.set('n', '<space>fg', builtin.live_grep, opts)
    vim.keymap.set('n', '<space>fb', builtin.buffers, opts)
    vim.keymap.set('n', '<space>fh', builtin.help_tags, opts)
    vim.keymap.set('n', '<space>fd', builtin.diagnostics, opts)
    vim.keymap.set('n', '<space>fk', builtin.keymaps, opts)
else
    vim.notify('Telescope plugin not found, not assigning keymaps', vim.log.levels.ERROR, nil)
end

-- nvimtree
--vim.keymap.del('n', '<F2>')
vim.keymap.set('n', '<F3>', ':NvimTreeToggle<CR>', opts)


-- neogen
vim.keymap.set("n", "<space>nf", ":lua require('neogen').generate()<CR>", opts)
vim.keymap.set("n", "<space>nc", ":lua require('neogen').generate({ type = 'class' })<CR>", opts)

local cmake_tools_ok, _ = pcall(require, 'cmake-tools')
if cmake_tools_ok then
    vim.keymap.set({ 'n' }, '<F33>', ':CMakeBuild<CR>', opts)                  -- Ctrl+F9
    vim.keymap.set({ 'n' }, '<F22>', ':CMakeRun<CR>', opts)                    -- Shift+F10
    vim.keymap.set({ 'n' }, '<M-S-F9>', ':CMakeSelectBuildType<CR>', opts)     -- Alt+Shift+F9
    vim.keymap.set({ 'n' }, '<M-S-F10>', ':CMakeSelectLaunchTarget<CR>', opts) -- Alt+Shift+F10
    vim.keymap.set({ 'n' }, '<M-s>', ':CMakeSettings<CR>', opts)
    vim.keymap.set({ 'n' }, '<M-S>', ':CMakeTargetSettings<CR>', opts)
else
    vim.notify('CMake Tools plugin not found, not assigning keymaps', vim.log.levels.ERROR, nil)
end
