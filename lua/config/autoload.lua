-- highlight searchs only when searching
vim.cmd([[
augroup vimrc-incsearch-highlight
autocmd!
autocmd CmdlineEnter /,\? :set hlsearch
autocmd CmdlineLeave /,\? :set nohlsearch
augroup END
]])


-- automatically sync packer after writing to plugins.lua
vim.cmd([[
augroup packer_user_config
autocmd!
autocmd BufWritePost plugins.lua source <afile> | PackerSync
augroup end
]])

-- add more extensions to glsl filetype
vim.cmd([[
augroup filetype
autocmd!
autocmd BufRead,BufNewFile *.vert,*.tesc,*.tese,*.geom,*.frag,*.comp set filetype=glsl
augroup end
]])

-- save buffer before switching from it
vim.cmd([[
augroup buffer
autocmd!
autocmd BufLeave,InsertLeave * silent! update
augroup end
]])
