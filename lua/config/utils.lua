local constants = require('config.constants')


local M = {}

M.split_str = function(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end

    local t = {}
    for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
        table.insert(t, str)
    end
    return t
end

M.find_executable = function()
    local progname = vim.fn.fnamemodify(vim.fn.getcwd(), ':t')
    local phandle = io.popen("find " .. vim.fn.getcwd() .. " -type f -executable -name " .. progname)
    local found_executables = {}

    if phandle == nil then
        io.stderr:write('failed to execute find command to search for executables')
        return nil
    end

    local cmd_output = phandle:read()
    phandle:close()

    if cmd_output == nil then
        io.stderr:write('find did not return any files')
        return nil
    end

    found_executables = M.split_str(cmd_output, '\n')

    for _, exe in ipairs(found_executables) do
        if string.find(string.lower(exe), 'debug') then
            return exe
        end
    end

    -- return the first found
    return found_executables[1]
end

M.file_exists = function(file)
    local f = io.open(file)
    if f ~= nil then
        f:close()
        return true
    end
    return false
end

-- checks for dap configuration in the current directory and applies the
-- configuration if found
-- @param config passed by mason-nvim-dap
-- @return true if found and applied a configuration, false otherwise
M.apply_project_dap_config = function(config)
    for _, file in ipairs(constants.DAP_CONFIG_PATHS) do
        if M.file_exists(file) then
            vim.notify("Reading DAP config from project", vim.log.levels.INFO)
            dofile(file)(config)
            return true
        end
    end
    return false
end

return M
