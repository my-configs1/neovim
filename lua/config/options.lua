--
-- vim options
--
local options = {
    autowriteall = true,
    backup = false,
    breakindent = true,
    completeopt = { "menu", "menuone", "preview" },
    confirm = true,
    cmdheight = 1,
    encoding = "utf-8",
    equalalways = false,
    expandtab = true,
    fileencoding = "utf-8",
    fileencodings = "utf-8,default,ucs-bom,latin1",
    hlsearch = false,
    ignorecase = false,
    mouse = 'a',
    mousefocus = false,
    number = true,
    relativenumber = true,
    shiftwidth = 4,
    signcolumn = 'auto:3',
    smartcase = false,
    smartindent = true,
    softtabstop = -1,
    splitbelow = true,
    splitright = true,
    tabstop = 4,
    termbidi = true,
    termguicolors = true,
    wrap = false,
}

vim.opt.iskeyword:append(':')

for k, v in pairs(options) do
    vim.opt[k] = v
end


--
-- netrw options
--

-- disable netrw
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
