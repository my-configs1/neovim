local ensure_packer = function()
    local install_path = vim.fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
    if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
        vim.fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
        vim.cmd([[packadd packer.nvim]])
        return true
    end
    return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup({
    function(use)
        use 'wbthomason/packer.nvim'

        --
        -- ui
        --
        use {
            'drewtempelmeyer/palenight.vim',
            disable = true,
        }
        use {
            'Everblush/nvim', as = 'everblush',
            disable = true,
        }
        use {
            'Mofiqul/vscode.nvim',
            disable = false,
        }
        use {
            'nvim-lualine/lualine.nvim',
            requires = { 'nvim-tree/nvim-web-devicons', }
        }
        use {
            'akinsho/bufferline.nvim', tag = '*',
            requires = { 'nvim-tree/nvim-web-devicons' }
        }
        use {
            "Fildo7525/pretty_hover",
        }

        --
        -- Completion
        --
        use {
            'hrsh7th/cmp-nvim-lsp',
            requires = {
                'hrsh7th/nvim-cmp',
            },
        }
        use {
            'hrsh7th/cmp-buffer',
            requires = {
                'hrsh7th/nvim-cmp',
            }
        }
        use {
            'hrsh7th/cmp-path',
            requires = {
                'hrsh7th/nvim-cmp',
            }
        }
        use {
            'hrsh7th/cmp-cmdline',
            requires = {
                'hrsh7th/nvim-cmp',
            }
        }
        use {
            "ray-x/lsp_signature.nvim",
        }
        use {
            'hrsh7th/cmp-nvim-lsp-signature-help',
            requires = {
                'hrsh7th/nvim-cmp',
            }
        }
        use {
            'saadparwaiz1/cmp_luasnip',
            requires = {
                'hrsh7th/nvim-cmp',
                { 'L3MON4D3/LuaSnip',                run = 'make install_jsregexp', },
                { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate', },
            }
        }
        use {
            'petertriho/cmp-git',
            requires = 'nvim-lua/plenary.nvim'
        }
        use {
            'windwp/nvim-autopairs',
        }


        --
        -- lsp
        --
        use {
            'williamboman/mason-lspconfig.nvim',
            requires = {
                'williamboman/mason.nvim',
                'neovim/nvim-lspconfig'
            }
        }
        use {
            'jay-babu/mason-null-ls.nvim',
            requires = {
                'williamboman/mason.nvim',
                {
                    'jose-elias-alvarez/null-ls.nvim',
                    requires = { 'nvim-lua/plenary.nvim' }
                },
            }
        }
        use {
            'p00f/clangd_extensions.nvim',
            requires = {
                'neovim/nvim-lspconfig'
            }
        }
        use {
            'ranjithshegde/ccls.nvim',
            requires = {
                'neovim/nvim-lspconfig'
            }
        }
        use {
            'folke/neodev.nvim',
            requires = {
                'hrsh7th/nvim-cmp',
            },
        }
        use { 'kosayoda/nvim-lightbulb' }

        --
        -- dap
        --
        use {
            'rcarriga/nvim-dap-ui',
            requires = { 'mfussenegger/nvim-dap' }
        }
        use {
            'jay-babu/mason-nvim-dap.nvim',
            requires = {
                'williamboman/mason.nvim',
                'mfussenegger/nvim-dap',
            }
        }

        --
        -- miscellaneous
        --
        use {
            'andymass/vim-matchup',
            requires = {
                { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate', },
            }
        }

        use {
            'onsails/lspkind.nvim',
        }

        use {
            'nvim-telescope/telescope.nvim',
            branch = '0.1.x',
            requires = {
                { 'nvim-lua/plenary.nvim' }
            }
        }


        -- commenting
        use {
            'numToStr/Comment.nvim',
            requires = {
                'JoosepAlviste/nvim-ts-context-commentstring',
            }
        }
        use {
            'JoosepAlviste/nvim-ts-context-commentstring',
            requires = {
                { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate', },
            }
        }
        use {
            "danymat/neogen",
            requires = {
                { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate', },
            }
        }

        -- git
        use { 'lewis6991/gitsigns.nvim' }

        -- file explorer
        use {
            'nvim-tree/nvim-tree.lua',
            requires = {
                'nvim-tree/nvim-web-devicons'
            }
        }

        -- window resizing
        use {
            'kwkarlwang/bufresize.nvim',
        }

        -- build systems
        use {
            'Civitasv/cmake-tools.nvim'
        }

        if packer_bootstrap then
            require('packer').sync()
        end

        require('config.plugconf')
    end,
    config = {
        autoremove = true,
    }
})
