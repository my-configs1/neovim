return function(capabilities, on_attach)
    require('lspconfig').glslls.setup({
        capabilities = capabilities,
        on_attach = on_attach,
        cmd = {
            'glslls',
            '--stdin',
            '--target-env=opengl',
        },
        root_dir = require('lspconfig').util.root_pattern('compile_commands.json', '.git')
    })
end
