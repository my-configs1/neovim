return function(capabilities, default_on_attach_func)
    capabilities.offsetEncoding = { "utf-32" }
    local ccls_ok, ccls = pcall(require, 'ccls')

    if ccls_ok then
        ccls.setup({
        })
    end
    require('lspconfig').ccls.setup({})
end
