return function(capabilities, default_on_attach_func)
    capabilities.offsetEncoding = { "utf-32" }

    local clangd_extensions_ok, clangd_extensions = pcall(require, 'clangd_extensions')
    local clangd_opts = {
        capabilities = capabilities,
        on_attach = default_on_attach_func,
        cmd = {
            'clangd',
            '--all-scopes-completion=true',
            '--background-index',
            '--clang-tidy',
            '--completion-parse=auto',
            '--completion-style=bundled',
            '--enable-config',
            '--fallback-style=Microsoft',
            '--function-arg-placeholders=false',
            '--header-insertion=never',
            '--header-insertion-decorators=true',
            '--include-cleaner-stdlib',
            '-j=4',
            '--limit-references=2000',
            '--limit-results=200',
            '--malloc-trim',
            '--rename-file-limit=0',
            '--pch-storage=memory',
        },
        init_options = {
            usePlaceholders = true,
            completeUnimported = true,
            semanticHighlighting = false,
        },
    }

    if clangd_extensions_ok then
        clangd_opts.on_attach = function(client, bufnr)
            default_on_attach_func(client, bufnr)

            local inlay_hints = require('clangd_extensions.inlay_hints')
            inlay_hints.setup_autocmd()
            inlay_hints.set_inlay_hints()
        end

        clangd_extensions.setup({
            inlay_hints = {
                other_hints_prefix = '≫',
                parameter_hints_prefix = '≪',
            },
            ast = {
                role_icons = {
                    type = "",
                    declaration = "",
                    expression = "",
                    specifier = "",
                    statement = "",
                    ["template argument"] = "",
                },
                kind_icons = {
                    Compound = "",
                    Recovery = "",
                    TranslationUnit = "",
                    PackExpansion = "",
                    TemplateTypeParm = "",
                    TemplateTemplateParm = "",
                    TemplateParamObject = "",
                },
            },
        })
    else
        vim.notify('clangd_extensions is missing', vim.log.levels.WARN, nil)
    end
    require('lspconfig').clangd.setup(clangd_opts)
end
