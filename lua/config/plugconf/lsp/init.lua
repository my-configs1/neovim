--
-- configure diagnostics
--
local signs = {
    { name = "DiagnosticSignError", text = "" },
    { name = "DiagnosticSignWarn", text = "" },
    { name = "DiagnosticSignHint", text = "" },
    { name = "DiagnosticSignInfo", text = "" },
}

for _, sign in ipairs(signs) do
    vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
end

local diagnostics_config = {
    virtual_text = {
        source = false,
        spacing = 2,
        --    default is nice
        --    prefix = "≫",
        format = function(diagnostic)
            return string.format('%s (%s)', diagnostic.message, diagnostic.source)
        end,
    },
    float = {
        format = function(diagnostic)
            return string.format('%s (%s)', diagnostic.message, diagnostic.source)
        end
    },
    signs = true,
    severity_sort = true,
    update_on_insert = true,
}
vim.diagnostic.config(diagnostics_config)

--
-- helper functions
--

-- set keybindings on lsp attach
local function lsp_keymaps(bufnr)
    local prettyhover_ok, prettyhover = pcall(require, 'pretty_hover')

    local opts = { buffer = bufnr, noremap = true, silent = true }
    vim.keymap.set("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
    vim.keymap.set("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
    if prettyhover_ok then
        vim.keymap.set("n", "<Space>k", function() prettyhover.hover() end, opts)
    else
        vim.keymap.set("n", "<Space>k", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
    end
    vim.keymap.set("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
    vim.keymap.set("n", "<Space>h", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
    vim.keymap.set("n", "<Space>rn", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
    vim.keymap.set("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
    vim.keymap.set("n", "<Space>a", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
    vim.keymap.set("n", "<Space>f", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
    vim.keymap.set("n", "[d", '<cmd>lua vim.diagnostic.goto_prev({ border = "rounded" })<CR>', opts)
    vim.keymap.set("n", "gl", '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
    vim.keymap.set("n", "]d", '<cmd>lua vim.diagnostic.goto_next({ border = "rounded" })<CR>', opts)
    vim.keymap.set("n", "<Space>q", "<cmd>lua vim.diagnostic.setloclist()<CR>", opts)
    vim.keymap.set("n", "<M-l>", ":Format<CR>", opts)
    vim.cmd [[ command! Format execute 'lua vim.lsp.buf.format({async = true})' ]]
end

-- setup lsp signature plugin
local function setup_lsp_signature(bufnr)
    local lsp_signature_ok, lsp_signature = pcall(require, 'lsp_signature')
    if not lsp_signature_ok then
        vim.notify('lsp_signature is missing', vim.log.levels.WARN, nil)
        return
    end

    if vim.bo.filetype == 'glsl' then
        return
    end

    lsp_signature.on_attach({
        hint_inline = function() return vim.fn.has("nvim-0.10") == 1 end,
        wrap = true,
    }, bufnr)
end

-- default function to be called on lsp attach
local function default_on_attach_func(client, bufnr)
    client.server_capabilities.semanticTokensProvider = nil
    lsp_keymaps(bufnr)
    setup_lsp_signature(bufnr)
end

--
-- configure servers
--

local capabilities = vim.lsp.protocol.make_client_capabilities()
local mason_ok, mason = pcall(require, 'mason')
local cmp_nvim_lsp_ok, cmp_nvim_lsp = pcall(require, 'cmp_nvim_lsp')

capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.resolveSupport = {
    properties = { "documentation", "detail", "additionalTextEdits" },
}

if cmp_nvim_lsp_ok then
    capabilities = cmp_nvim_lsp.default_capabilities(capabilities)
end

if not mason_ok then
    vim.notify('mason is missing', vim.log.levels.WARN, nil)
    return
end

mason.setup({})

local mason_lspconfig_ok, mason_lspconfig = pcall(require, 'mason-lspconfig')
if not mason_lspconfig_ok then
    vim.notify('mason-lspconfig is missing', vim.log.levels.WARN, nil)
    return
end

mason_lspconfig.setup({
    ensure_installed = { 'ansiblels', 'asm_lsp', 'bashls', 'pkgbuild_language_server',
        'clangd', 'cmake', 'dockerls', 'dotls', 'gradle_ls', 'helm_ls', 'jsonls',
        'jdtls', 'julials', 'texlab', 'lua_ls', 'marksman', 'opencl_ls', 'pyre',
        'sqlls', 'yamlls' },
    automatic_installation = true,
    handlers = {
        function(server_name)
            require('lspconfig')[server_name].setup({
                capabilities = capabilities,
                on_attach = default_on_attach_func,
            })
        end,
        ['lua_ls'] = function()
            require('config.plugconf.lsp.servers.lua')(capabilities, default_on_attach_func)
        end,
        ['clangd'] = function()
            require('config.plugconf.lsp.servers.clangd')(capabilities, default_on_attach_func)
        end,
    },
})

--require('config.plugconf.lsp.servers.ccls')(capabilities, default_on_attach_func)
require('config.plugconf.lsp.servers.glsl-ls')(capabilities, default_on_attach_func)
