local nvim_autopairs_ok, nvim_autopairs = pcall(require, 'nvim-autopairs')
if not nvim_autopairs_ok then
    vim.notify('nvim-autopairs is missing', vim.log.levels.WARN, nil)
    return
end

local cond = require("nvim-autopairs.conds")
local Rule = require("nvim-autopairs.rule")

nvim_autopairs.setup({
    check_ts = true,
    disable_filetype = { "TelescopePrompt" },
})

nvim_autopairs.add_rules({
    Rule("<", ">"):with_pair(cond.before_regex("%a+")):with_move(function(opts)
        return opts.char == ">"
    end),
})
