local telescope_ok, telescope = pcall(require, 'telescope')
if not telescope_ok then
    vim.notify('telescope is missing', vim.log.levels.WARN, nil)
    return
end

telescope.setup({})
