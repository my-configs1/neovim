local nvim_lightbulb_ok, nvim_lightbulb = pcall(require, 'nvim-lightbulb')
if not nvim_lightbulb_ok then
    vim.notify('nvim-lightbulb is missing', vim.log.levels.WARN, nil)
    return
end

nvim_lightbulb.setup({
    hide_in_unfocused_buffer = false,
    autocmd = { enabled = true },
    ignore = {
        actions_without_kind = false,
        clients = { 'null-ls' },
    }
})
