-- colorscheme
require('config.plugconf.ui')

-- completion engine
require('config.plugconf.autopairs')
require('config.plugconf.cmp')

-- treesitter
require('config.plugconf.treesitter')

-- matchup
require('config.plugconf.matchup')

-- neovim
require('config.plugconf.neodev')

-- lsp
require('config.plugconf.lsp')
require('config.plugconf.lightbulb')
require('config.plugconf.null_ls')

-- dap
require('config.plugconf.dap')

-- telescope
require('config.plugconf.telescope')

-- comment
require('config.plugconf.comment')

-- git
require('config.plugconf.gitsigns')

-- file explorer
require('config.plugconf.nvimtree')

-- buffer resize
require('config.plugconf.bufresize')

-- prettyhover
require('config.plugconf.prettyhover')

-- neogen
require('config.plugconf.neogen')

-- cmake
require('config.plugconf.cmake_tools')
