local neogen_ok, neogen = pcall(require, 'neogen')
if not neogen_ok then
    vim.notify('neogen is missing', vim.log.levels.WARN)
    return
end

neogen.setup({ snippet_engine = "luasnip" })
