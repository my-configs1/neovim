return function(config)
    config.adapters = {
        type = "executable",
        command = '/usr/bin/lldb-vscode',
        name = 'codelldb',
    }
    config.configurations = {
        {
            name = 'Launch',
            type = 'codelldb',
            request = 'launch',
            program = require('config.utils').find_executable,
            cwd = '${workspaceFolder}',
            stopOnEntry = false,
            args = {},
        },
    }
    config.filetypes = { 'c', 'cpp' }
end
