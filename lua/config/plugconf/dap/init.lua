local utils = require('config.utils')
local constants = require('config.constants')

local dap_ok, dap = pcall(require, 'dap')
if not dap_ok then
    vim.notify('dap is missing', vim.log.levels.WARN, nil)
    return
end

local dapui_ok, dapui = pcall(require, 'dapui')
if not dapui_ok then
    vim.notify('dapui is missing', vim.log.levels.WARN, nil)
    return
end

local mason_nvim_dap_ok, mason_nvim_dap = pcall(require, 'mason-nvim-dap')
if not mason_nvim_dap_ok then
    vim.notify('mason-nvim-dap is missing', vim.log.levels.WARN, nil)
    return
end

--
-- configuration for mason nvim
--
local mason_nvim_config = {
    ensure_installed = { 'codelldb' },
    handlers = {
        function(config)
            dap.handlers = {}
            dap.configurations = {}

            utils.apply_project_dap_config(config)
            mason_nvim_dap.default_setup(config)
        end,
        codelldb = function(config)
            dap.handlers = {}
            dap.configurations = {}

            if not utils.apply_project_dap_config(config) then
                require('config.plugconf.dap.servers.codelldb')(config)
            end
            mason_nvim_dap.default_setup(config)
        end,
    }
}

mason_nvim_dap.setup(mason_nvim_config)

-- create an autocmd to reload configuration when project dap config file
-- changes
vim.api.nvim_create_augroup('DapConfigReload', { clear = true })
for _, file in ipairs(constants.DAP_CONFIG_PATHS) do
    vim.api.nvim_create_autocmd('BufWritePost', {
        group = 'DapConfigReload',
        pattern = file,
        callback = function() mason_nvim_dap.setup(mason_nvim_config) end,
    })
end

--
-- configure dap ui
--
dapui.setup({})

local function open_dapui()
    dapui.open()
    vim.cmd(vim.api.nvim_replace_termcodes('normal <C-w>=', true, true, true))
end

local function close_dapui()
    dapui.close()
    vim.cmd(vim.api.nvim_replace_termcodes('normal <C-w>=', true, true, true))
end

dap.listeners.after.event_initialized['dapui_config'] = open_dapui
dap.listeners.before.event_terminated['dapui_config'] = close_dapui
dap.listeners.before.event_exited['dapui_config'] = close_dapui

vim.fn.sign_define('DapBreakpoint', { text = '🔴', texthl = '', linehl = '', numhl = '' })
vim.fn.sign_define('DapStopped', { text = '▶️', texthl = '', linehl = '', numhl = '' })
