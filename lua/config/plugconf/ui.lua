--
-- Configuration for colorscheme
--

local everblush_ok, everblush = pcall(require, 'everblush')
if everblush_ok then
    local p = {
        color0 = '#fafafa',
        color8 = '#bfbfbf',
    }

    everblush.setup({
        nvim_tree = {
            contrast = true,
        },
        override = {
            LineNr = { fg = p.color0 },
            VertSplit = { bg = p.color0 },
            Visual = { bg = p.color0 },
            IndentBlanklineChar = { fg = p.color0 },
            NvimTreeIndentMarker = { fg = p.color0 },
            ["@text.warning"] = { fg = p.color0 },
            TelescopeBorder = { fg = p.color0 },
            ["@exception"] = { fg = p.color8 },
            ["@float"] = { fg = p.color8 },
            ["@text.danger"] = { fg = p.color8 },
            ["@text.note"] = { fg = p.color8 },
        },
        transparent_background = true,
    })

    vim.g.terminal_color_0 = p.color0
    vim.g.terminal_color_8 = p.color8

    vim.cmd.colorscheme('everblush')

    local lualine_ok, lualine = pcall(require, 'lualine')
    if lualine_ok then
        lualine.setup({
            options = { theme = 'everblush' }
        })
    else
        vim.notify('lualine plugin not found', vim.log.levels.WARN, nil)
    end
end

local vscode_ok, vscode = pcall(require, 'vscode')
if vscode_ok then
    vscode.setup({
        style = 'dark',
        italic_comments = true,
    })
    vscode.load()

    local lualine_ok, lualine = pcall(require, 'lualine')
    if lualine_ok then
        lualine.setup({
            options = { theme = 'vscode' }
        })
    else
        vim.notify('lualine plugin not found', vim.log.levels.WARN, nil)
    end
end

local bufferline_enable = false
local bufferline_ok, bufferline = pcall(require, 'bufferline')

if bufferline_enable and bufferline_ok then
    bufferline.setup({
        options = {
            buffer_close_icon = "x",
            close_command = "bdelete %d",
            close_icon = "x",
            indicator = {
                style = "icon",
                icon = " ",
            },
            left_trunc_marker = "",
            modified_icon = "●",
            offsets = { { filetype = "NvimTree", text = "EXPLORER", text_align = "center" } },
            right_mouse_command = "bdelete! %d",
            right_trunc_marker = "",
            show_close_icon = false,
            show_tab_indicators = true,
        },
        highlights = {
            fill = {
                fg = { attribute = "fg", highlight = "Normal" },
                bg = { attribute = "bg", highlight = "StatusLineNC" },
            },
            background = {
                fg = { attribute = "fg", highlight = "Normal" },
                bg = { attribute = "bg", highlight = "StatusLine" },
            },
            buffer_visible = {
                fg = { attribute = "fg", highlight = "Normal" },
                bg = { attribute = "bg", highlight = "Normal" },
            },
            buffer_selected = {
                fg = { attribute = "fg", highlight = "Normal" },
                bg = { attribute = "bg", highlight = "Normal" },
            },
            separator = {
                fg = { attribute = "bg", highlight = "Normal" },
                bg = { attribute = "bg", highlight = "StatusLine" },
            },
            separator_selected = {
                fg = { attribute = "fg", highlight = "Special" },
                bg = { attribute = "bg", highlight = "Normal" },
            },
            separator_visible = {
                fg = { attribute = "fg", highlight = "Normal" },
                bg = { attribute = "bg", highlight = "StatusLineNC" },
            },
            close_button = {
                fg = { attribute = "fg", highlight = "Normal" },
                bg = { attribute = "bg", highlight = "StatusLine" },
            },
            close_button_selected = {
                fg = { attribute = "fg", highlight = "Normal" },
                bg = { attribute = "bg", highlight = "Normal" },
            },
            close_button_visible = {
                fg = { attribute = "fg", highlight = "Normal" },
                bg = { attribute = "bg", highlight = "Normal" },
            },
        },
    })
end
