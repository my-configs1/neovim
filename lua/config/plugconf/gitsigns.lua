local gitsigns_ok, gitsigns = pcall(require, 'gitsigns')
if not gitsigns_ok then
    vim.notify('gitsigns is missing', vim.log.levels.WARN, nil)
    return
end

gitsigns.setup({
    signs = {
        add          = { hl = "GitSignsAdd", text = "▎", numhl = "GitSignsAddNr", linehl = "GitSignsAddLn" },
        change       = { hl = "GitSignsChange", text = "▎", numhl = "GitSignsChangeNr", linehl = "GitSignsChangeLn" },
        delete       = { hl = "GitSignsDelete", text = "_", numhl = "GitSignsDeleteNr", linehl = "GitSignsDeleteLn" },
        topdelete    = { hl = "GitSignsDelete", text = "‾", numhl = "GitSignsDeleteNr", linehl = "GitSignsDeleteLn" },
        changedelete = { hl = "GitSignsChange", text = "▎", numhl = "GitSignsChangeNr", linehl = "GitSignsChangeLn" },
        untracked    = { text = '┆', numhl = "GitSignsUntrackedNr", linehl = "GitSignsUntrackedLn" },
    },
    on_attach = function(bufnr)
        local opts = { buffer = bufnr, silent = true, noremap = true }
        vim.keymap.set('n', ']c', function()
            if vim.wo.diff then return ']c' end
            vim.schedule(function() gitsigns.next_hunk() end)
            return '<Ignore>'
        end, opts)

        vim.keymap.set('n', '[c', function()
            if vim.wo.diff then return '[c' end
            vim.schedule(function() gitsigns.prev_hunk() end)
            return '<Ignore>'
        end, opts)

        vim.keymap.set('n', '<leader>hs', gitsigns.stage_hunk)
        vim.keymap.set('n', '<leader>hr', gitsigns.reset_hunk)
        vim.keymap.set('v', '<leader>hs', function() gitsigns.stage_hunk { vim.fn.line('.'), vim.fn.line('v') } end)
        vim.keymap.set('v', '<leader>hr', function() gitsigns.reset_hunk { vim.fn.line('.'), vim.fn.line('v') } end)
        vim.keymap.set('n', '<leader>hS', gitsigns.stage_buffer)
        vim.keymap.set('n', '<leader>hu', gitsigns.undo_stage_hunk)
        vim.keymap.set('n', '<leader>hR', gitsigns.reset_buffer)
        vim.keymap.set('n', '<leader>hp', gitsigns.preview_hunk)
        vim.keymap.set('n', '<leader>hb', function() gitsigns.blame_line { full = true } end)
        vim.keymap.set('n', '<leader>tb', gitsigns.toggle_current_line_blame)
        vim.keymap.set('n', '<leader>hd', gitsigns.diffthis)
        vim.keymap.set('n', '<leader>hD', function() gitsigns.diffthis('~') end)
        vim.keymap.set('n', '<leader>td', gitsigns.toggle_deleted)
        vim.keymap.set({ 'o', 'x' }, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
    end
})
