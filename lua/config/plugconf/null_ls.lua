local null_ls_ok, null_ls = pcall(require, 'null-ls')
if null_ls_ok then
    null_ls.setup({})
else
    vim.notify('null-ls is missing', vim.log.levels.WARN, nil)
    return
end

null_ls.setup({
    -- handled by diagnostics configuration before lsp
    --diagnostics_format = "#{m} (#{s})",
    sources = {
        -- code actions
        null_ls.builtins.code_actions.gitsigns,
        -- diagnostics
        null_ls.builtins.diagnostics.codespell,
        null_ls.builtins.diagnostics.cppcheck.with({
            args = {
                --'--enable=warning,style,performance,portability',
                'enable=all',
                "--template=gcc",
                '-j4',
                '--max-ctu-depth=10',
                '--language=' .. (vim.bo.filetype == 'c' and 'c' or 'c++'),
                '$FILENAME',
            }, --"--enable=warning,style,performance,portability", "--template=gcc", "$FILENAME"
        }),
        null_ls.builtins.diagnostics.clang_check,
        null_ls.builtins.diagnostics.yamllint.with({
            extra_args = {
                '-d',
                'relaxed',
            }
        }),
        -- formatting
        null_ls.builtins.formatting.clang_format,
        null_ls.builtins.formatting.cmake_format,
        null_ls.builtins.formatting.codespell,
        null_ls.builtins.formatting.prettier,
    },
    temp_dir = '/tmp',
    update_in_insert = true,
})

require("mason-null-ls").setup({
    ensure_installed = nil,
    automatic_installation = true,
})
