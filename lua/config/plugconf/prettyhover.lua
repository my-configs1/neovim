local prettyhover_ok, prettyhover = pcall(require, 'pretty_hover')
if not prettyhover_ok then
    vim.notify('pretty_hover is missing', vim.log.levels.WARN)
    return
end

prettyhover.setup({})
