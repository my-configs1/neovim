local neodev_ok, neodev = pcall(require, 'neodev')
if not neodev_ok then
    vim.notify('neodev is missing', vim.log.levels.WARN, nil)
    return
end

neodev.setup({})
