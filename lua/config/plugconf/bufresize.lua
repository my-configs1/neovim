local opts = { noremap = true, silent = true }

local bufresize_ok, bufresize = pcall(require, 'bufresize')
if not bufresize_ok then
    vim.notify('bufresize is missing', vim.log.levels.WARN, nil)
    return
end

bufresize.setup({
    register = {
        keys = {
            { 'n', '<C-w><',        '<C-w><',                  opts },
            { 'n', '<C-w>>',        '<C-w>>',                  opts },
            { 'n', '<C-w>+',        '<C-w>+',                  opts },
            { 'n', '<C-w>-',        '<C-w>-',                  opts },
            { 'n', '<C-w>_',        '<C-w>_',                  opts },
            { 'n', '<C-w>=',        '<C-w>=',                  opts },
            { 'n', '<C-w>|',        '<C-w>|',                  opts },
            { '',  '<LeftRelease>', '<LeftRelease>',           opts },
            { 'i', '<LeftRelease>', '<LeftRelease><C-o>',      opts },
            { 'n', '<C-Up>',        ':resize -2<CR>',          opts },
            { 'n', '<C-Down>',      ':resize +2<CR>',          opts },
            { 'n', '<C-Right>',     ':vertical resize -2<CR>', opts },
            { 'n', '<C-Left>',      ':vertical resize -2<CR>', opts },
        },
        trigger_events = {}  -- { 'BufWinEnter', 'WinEnter' },
    },
    resize = {
        keys = {},
        trigger_events = {}, -- { "VimResized" },
        increment = false,
    },
})
