local nvim_treesitter_ok, nvim_treesitter = pcall(require, 'nvim-treesitter')
if not nvim_treesitter_ok then
    vim.notify('nvim-treesitter is missing', vim.log.levels.WARN, nil)
    return
end

require('nvim-treesitter.configs').setup({
    -- A list of parser names, or "all" (the five listed parsers should always be installed)
    ensure_installed = { "c", "lua", "vim", "vimdoc", "query", "bash", "cpp", "cmake", "cuda", "dart",
        "dockerfile", "git_config", "git_rebase", "gitcommit", "gitignore", "glsl", "http", "ini",
        "java", "javascript", "jq", "jsonc", "julia", "latex", "make", "markdown_inline", "python", "regex",
        "sql", "yaml" },

    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = false,

    -- Automatically install missing parsers when entering buffer
    -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
    auto_install = true,

    -- List of parsers to ignore installing (for "all")
    -- ignore_install = { "javascript" },

    ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
    -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

    highlight = {
        enable = true,

        -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
        -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
        -- the name of the parser)
        -- list of language that will be disabled
        --disable = { "rust" },
        -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
        --disable = function(lang, buf)
        --    local max_filesize = 100 * 1024 -- 100 KB
        --    local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        --    if ok and stats and stats.size > max_filesize then
        --        return true
        --    end
        --end,

        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
    },
    indent = {
        enable = false,
    },
    matchup = {
        enable = true,
        include_match_words = true,
    },
    context_commentstring = {
        enable = true,
    },
})

--
-- vim folding options
--
vim.opt.foldenable = false                                -- Disable folding at startup
vim.opt.foldmethod = 'expr'
vim.opt.foldexpr   = vim.fn['nvim_treesitter#foldexpr']() -- nvim_treesitter#foldexpr()
